---
name: "Xiaomi Mi 8"
deviceType: "phone"

deviceInfo:
  - id: "cpu"
    value: "Octa-core (4x2.8 GHz Kryo 385 Gold & 4x1.8 GHz Kryo 385 Silver)"
  - id: "chipset"
    value: "Qualcomm SDM845 Snapdragon 845"
  - id: "gpu"
    value: "Adreno 630"
  - id: "rom"
    value: "64 GB,128 GB, 256 GB"
  - id: "ram"
    value: "6 GB, 8 GB"
  - id: "android"
    value: "Android 8.1 (Oreo), upgradable to Android 10, MIUI 12"
  - id: "battery"
    value: "Li-Po 3400 mAh, non-removable"
  - id: "display"
    value: "6.21 inches, 97.1 cm2 (~83.8% screen-to-body ratio), 1080 x 2248 pixels, 9:18.7 ratio (~402 ppi density)"
  - id: "rearCamera"
    value: '12 MP, f/1.8, 1/2.55", 1.4µm, dual pixel PDAF, 4-axis OIS 12 MP, f/2.4, 56mm (telephoto), 1/3.4", 1.0µm, AF, 2x optical zoom'
  - id: "frontCamera"
    value: '20 MP, f/2.0, (wide), 1/3", 0.9µm'
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "154.9 x 74.8 x 7.6 mm (6.10 x 2.94 x 0.30 in)"
  - id: "weight"
    value: "175 g (6.17 oz)"

contributors:
  - name: "Ismayil Taghi-Zada"
    forum: "https://forums.ubports.com/user/itagizade"

externalLinks:
  - name: "Device Repository"
    link: "https://github.com/ubports-dipper/xiaomi-dipper/"
  - name: "Kernel Repository"
    link: "https://github.com/ubports-dipper/kernel-xiaomi-sdm845"
  - name: "CI Builds"
    link: "https://github.com/ubports-dipper/xiaomi-dipper/actions/workflows/main.yml"
  - name: "Releases & ZIPs"
    link: "https://github.com/ubports-dipper/xiaomi-dipper/releases/latest"

communityHelp:
  - name: "Telegram Group - Ubuntu Touch Mi8 Community"
    link: "https://t.me/joinchat/IVwd1fJVDgFmMTVi"
---
